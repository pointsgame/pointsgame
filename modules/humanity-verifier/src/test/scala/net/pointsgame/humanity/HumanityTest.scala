package net.pointsgame.humanity

import net.pointsgame.humanity.Helper.{longPower => power}
import net.pointsgame.humanity.Numbers.base
import utest._


object HumanityTest extends TestSuite {
	val tests = TestSuite {

	'verifier_correctness {
		val verifierClass = new MyHumanVerifier()
		val mod = verifierClass.mod
		var power = 1
		var remainder = base
		while (remainder >= Numbers.acceptableLimit && !verifierClass.isHuman) {
			assert(power < 10000) // otherwise considered too long
			remainder = (remainder * base) % mod
			power += 1
			verifierClass.check(power)
		}
	}

	'power_operator_examples {
		assert(power(1, 1, 1000) == 1)
		assert(power(1, 2, 1000) == 1)
		assert(power(1, 10, 1000) == 1)

		assert(power(2, 1, 1000) == 2)
		assert(power(2, 2, 1000) == 4)
		assert(power(2, 10, 100000) == 1024)

		assert(power(2, 3, 8) == 0)

		assert(power(2, 2, 3) == 1)
		assert(power(2, 3, 3) == 2)
		assert(power(2, 4, 3) == 1)
		assert(power(2, 100, 3) == 1)
		assert(power(2, 101, 3) == 2)
		assert(power(2, 1000000000, 3) == 1)
	}
	}

}
